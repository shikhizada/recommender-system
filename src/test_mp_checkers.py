from src.parsers import *
from src.mp_checkers.log import *

from pm4py.objects.log.importer.xes import factory as xes_import_factory
log = xes_import_factory.apply(IN_LOG_PATH)


def run_all_mp_checkers():
    input = parse_decl(IN_DECL_PATH)
    activities = input.activities
    for key, conditions in input.checkers.items():
        if key.startswith(EXISTENCE):
            mp_existence_checker(log, True, activities[0], conditions[0], conditions[1])
        elif key.startswith(ABSENCE):
            mp_absence_checker(log, True, activities[0], conditions[0], conditions[1])
        elif key.startswith(INIT):
            mp_init_checker(log, True, activities[0], conditions[0])
        elif key.startswith(EXACTLY):
            mp_exactly_checker(log, True, activities[0], conditions[0], conditions[1])
        elif key.startswith(CHOICE):
            mp_choice_checker(log, True, activities[0], activities[1], conditions[0])
        elif key.startswith(EXCLUSIVE_CHOICE):
            mp_exclusive_choice_checker(log, True, activities[0], activities[1], conditions[0])
        elif key.startswith(RESPONDED_EXISTENCE):
            mp_responded_existence_checker(log, True, activities[0], activities[1], conditions[0], conditions[1])
        elif key.startswith(RESPONSE):
            mp_response_checker(log, True, activities[0], activities[1], conditions[0], conditions[1])
        elif key.startswith(ALTERNATE_RESPONSE):
            mp_alternate_response_checker(log, True, activities[0], activities[1], conditions[0], conditions[1])
        elif key.startswith(CHAIN_RESPONSE):
            mp_chain_response_checker(log, True, activities[0], activities[1], conditions[0], conditions[1])
        elif key.startswith(PRECEDENCE):
            mp_precedence_checker(log, True, activities[0], activities[1], conditions[0], conditions[1])
        elif key.startswith(ALTERNATE_PRECEDENCE):
            mp_alternate_precedence_checker(log, True, activities[0], activities[1], conditions[0], conditions[1])
        elif key.startswith(CHAIN_PRECEDENCE):
            mp_chain_precedence_checker(log, True, activities[0], activities[1], conditions[0], conditions[1])
        elif key.startswith(ALTERNATE_RESPONSE):
            mp_not_responded_existence_checker(log, True, activities[0], activities[1], conditions[0], conditions[1])
        elif key.startswith(NOT_RESPONSE):
            mp_not_response_checker(log, True, activities[0], activities[1], conditions[0], conditions[1])
        elif key.startswith(NOT_CHAIN_RESPONSE):
            mp_not_chain_response_checker(log, True, activities[0], activities[1], conditions[0], conditions[1])
        elif key.startswith(NOT_PRECEDENCE):
            mp_not_precedence_checker(log, True, activities[0], activities[1], conditions[0], conditions[1])
        elif key.startswith(NOT_CHAIN_PRECEDENCE):
            mp_not_chain_precedence_checker(log, True, activities[0], activities[1], conditions[0], conditions[1])

run_all_mp_checkers()
