from src.utils.helper import get_project_root


ROOT_DIR = str(get_project_root())
IN_LOG_PATH = ROOT_DIR + "/data/input/xes/log.xes"
IN_DECL_PATH = ROOT_DIR + "/data/input/decl/TestAl.decl"
OUT_PDF_DECISION_TREE_PATH = ROOT_DIR + "/data/output/pdf/decision_tree.pdf"